#!/bin/bash

cd /tmp || exit 1

case "$(uname -m)" in
  x86_64)
    ARCH_S6="x86_64"
    ;;
  aarch64)
    ARCH_S6="aarch64"
    ;;
  armv7l)
    ARCH_S6="armhf"
    ;;
  *)
    echo "unsupported arch"
    uname -m
    exit 1
    ;;
esac

echo "Downloading S6 Overlay ${S6_OVERLAY_VERSION} - ${ARCH_S6}"
wget https://github.com/just-containers/s6-overlay/releases/download/${S6_OVERLAY_VERSION}/s6-overlay-${ARCH_S6}.tar.xz -O s6-overlay.tar.xz